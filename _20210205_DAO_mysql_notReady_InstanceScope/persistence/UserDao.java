package _20210205_DAO_mysql_notReady_InstanceScope.persistence;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

import _20210205_DAO_mysql_notReady_InstanceScope.persistence.models.User;

public class UserDao implements Dao<User> {
    
    public UserDao() {

        DbController.connect();
        DbController.createUserTable();        

        this.save(new User("John", "john@domain.com"));
        this.save(new User("Susan", "susan@domain.com"));
    }
    
    public Optional<User> get(int id) {
        return DbController.selectOne(id);
    }
    
    public List<User> getAll() {
        return DbController.selectAll();
    }

    public void save(User user) {
        DbController.insertUser(user.getName(), user.getEmail());
    }
    
    public void update(User user, String[] params) {

        user.setName(Objects.requireNonNull(
          params[0], "Name cannot be null"));
        user.setEmail(Objects.requireNonNull(
          params[1], "Email cannot be null"));
        
        //von Tutorial korrigiert, dass User nicht doppelt in Liste
        //users.add(user);
    }
    
    public void delete(int id) {
        DbController.delete(id);
    }
}

class DbController {
    static Connection conn;

    public static void insertUser(String name, String email){
        String sql = "INSERT INTO users(name,email) VALUES(?,?)";
        try {
            PreparedStatement pstmt = conn.prepareStatement(sql);
            pstmt.setString(1, name);
            pstmt.setString(2, email);
            pstmt.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    public static void createUserTable() {
        String sql = "CREATE TABLE IF NOT EXISTS users (\n"
                + "	id integer PRIMARY KEY,\n"
                + "	name text NOT NULL,\n"
                + "	email text\n"
                + ");";
        try {
            Statement stmt = conn.createStatement();
            stmt.execute(sql);
            System.out.println("Table warehouse is created!");
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static void connect() {
        String url = "jdbc:sqlite:./src/_20210205_DAO_mysql_notReady_InstanceScope/db/mydb.db";
        try {
            conn = DriverManager.getConnection(url);            
            System.out.println("Connection to SQLite has been established.");
        } catch (SQLException e) {
            e.printStackTrace();
        }         
    }

    public static Optional<User> selectOne(int id){

        String sql = "SELECT name, email FROM users WHERE id = ?";
        User user = null;
        
        try{
            PreparedStatement pstmt = conn.prepareStatement(sql);
            pstmt.setInt(1, id);
            ResultSet rs = pstmt.executeQuery();
            
            // create user Object with result set
            if(rs.next()){
                user = new User(rs.getString("name"), rs.getString("email"));
            }
        } catch (SQLException e){
            e.printStackTrace();
        }
        return Optional.ofNullable(user);



        //ArrayList<User> users = selectAll();
        //return Optional.ofNullable(users.get(id));
    }

    public static ArrayList<User> selectAll(){
        String sql = "SELECT name, email FROM users";
        ArrayList<User> users = new ArrayList<User>();
        
        try{
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);
            
            // loop through the result set
            while (rs.next()) {
                User user = new User(rs.getString("name"), rs.getString("email"));
                users.add(user);
            }
        } catch (SQLException e){
            e.printStackTrace();
        }
        return users;
    }

    public static void delete(int id){
        String sql = "DELETE FROM users WHERE id = ?";

        try{
            PreparedStatement pstmt = conn.prepareStatement(sql);
            pstmt.setInt(1, id);
            pstmt.executeUpdate();

        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
    }
}