package _20210205_DAO_mysql_notReady_InstanceScope.persistence;

public class DaoFactory {
    public static Dao main(DaoType name){
        switch (name) {
            case MONSTER:
                return new MonsterDao();
        
            case USER:
                return new UserDao();
            default:
                return null;
        }
    }
}